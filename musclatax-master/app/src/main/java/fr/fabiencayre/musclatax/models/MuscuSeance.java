package fr.fabiencayre.musclatax.models;

import android.app.Service;
import android.content.ContentValues;

import java.io.Serializable;
import java.util.Map;

import fr.fabiencayre.musclatax.utils.database.Database;
import fr.fabiencayre.musclatax.utils.database.Savable;

public class MuscuSeance extends Seance implements Savable, Serializable {

    private int nbReps;
    private int tempsRepos;

    public MuscuSeance(Map<String, Object> map) {
        super(map);
        this.nbReps = (int) map.get("nombreDeReps");
        this.nbReps = (int) map.get("tempsRepos");
    }

    public MuscuSeance(){
        super();
    }

    public void setNbReps(int nbReps) {
        this.nbReps = nbReps;
    }

    public void setTempsRepos(int tempsRepos) {
        this.tempsRepos = tempsRepos;
    }

    public int getNbReps() {
        return nbReps;
    }

    public int getTempsRepos() {
        return tempsRepos;
    }

    @Override
    public void save(ContentValues values) {
        super.save(values);
        values.put("nombreDeReps", this.nbReps);
        values.put("tempsRepos", this.tempsRepos);
    }
}
