package fr.fabiencayre.musclatax.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import fr.fabiencayre.musclatax.R;


public class TextLabeledInput extends LinearLayout {

    private TextView textView;
    private EditText textField;

    public TextLabeledInput(Context context, AttributeSet set) {
        super(context, set);
        inflate(context, R.layout.text_label_input, this);

        this.textView = findViewById(R.id.tli_TextView);
        this.textField = findViewById(R.id.tli_TextField);

        this.textView.setText(set.getAttributeValue("app", "textDisplay"));
        this.textField.setText(set.getAttributeValue("app", "textView"));
    }


    public EditText getTextField() {
        return textField;
    }

    public TextView getTextView() {
        return textView;
    }
}
