package fr.fabiencayre.musclatax.utils;

import java.text.DecimalFormat;

public final class Utils {

    private static DecimalFormat d2f = new DecimalFormat("##.##");

    private Utils(){}

    public static String formatDouble(double d){
        return d2f.format(d);
    }

    public static boolean isNumber(String string){
        try{
            Integer.parseInt(string);
            return true;
        }catch (Exception e){return false;}
    }

}
