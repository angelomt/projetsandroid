package fr.fabiencayre.musclatax;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import fr.fabiencayre.musclatax.controllers.listeners.main.CreateSeanceListener;
import fr.fabiencayre.musclatax.models.AdapteurSeance;
import fr.fabiencayre.musclatax.models.Seance;
import fr.fabiencayre.musclatax.utils.DateUtils;
import fr.fabiencayre.musclatax.utils.Pair;
import fr.fabiencayre.musclatax.utils.Utils;
import fr.fabiencayre.musclatax.utils.database.Database;

public class StatisticActivity extends AppCompatActivity {

    ListView liste;
    TextView stats1;
    TextView stats2;
    TextView stats3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityCourse();
    }

    public void activityCourse(){
        setContentView(R.layout.activity_statistic_course);
        this.liste =  findViewById(R.id.listeSeanceCourse);
        this.stats1 = findViewById(R.id.nb_kms_parcourus);
        this.stats2 = findViewById(R.id.vitesse_moyenne_totale);
        this.stats3 = findViewById(R.id.course_durée_moyenne);

        Database db = new Database(this);
        String req = getResources().getString(R.string.SQL_STATS_COURSE);
        long currentTime = System.currentTimeMillis();
        Map<String, Object> result = db.querySingle(req, currentTime, currentTime, currentTime);
        SimpleDateFormat dateFormat = new SimpleDateFormat(getResources().getString(R.string.date_format), Locale.FRENCH);

        int totKm = 0;
        double moyVitesse = 0;
        double moyDuree = 0;
        if(result.containsKey("totKm") && result.get("totKm") != null){
            totKm = (int) result.get("totKm");
            moyVitesse = (double) result.get("moyVitesse");
            moyDuree = (double) result.get("moyDuree");
        }
        moyVitesse*=3600; // km/s => km/h
        this.stats1.setText(String.valueOf(totKm));
        this.stats2.setText(Utils.formatDouble(moyVitesse));
        this.stats3.setText(dateFormat.format(moyDuree*1000));
    }

    public void activityMuscu(){
        setContentView(R.layout.activity_statistic_muscu);
        this.liste =  findViewById(R.id.listeSeanceMuscu);
        this.stats1 = findViewById(R.id.nb_reps);
        this.stats2 = findViewById(R.id.duree_totale);
        this.stats3 = findViewById(R.id.exercice_durée_totale);

        Database db = new Database(this);
        String req = getResources().getString(R.string.SQL_STATS_MUSCU);
        long currentTime = System.currentTimeMillis();
        Map<String, Object> result = db.querySingle(req, currentTime, currentTime, currentTime);
        int nbTotReps = 0;
        int sommeDuree = 0;
        int sommeSeance = 0;
        if(!result.isEmpty()){
            if(result.containsKey("nbTotReps") && result.get("nbTotReps") != null){
                nbTotReps = (int) result.get("nbTotReps");
                sommeDuree = (int) result.get("sommeDuree");
                sommeSeance = (int) result.get("sommeSeance");
            }
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(getResources().getString(R.string.date_format), Locale.FRENCH);
        this.stats1.setText(String.valueOf(nbTotReps));
        this.stats2.setText(dateFormat.format(sommeDuree * 1000));
        this.stats3.setText(dateFormat.format(sommeSeance * 1000));

        String req2 = getResources().getString(R.string.SQL_STATS_MUSCU_LISTE);
        List<Map<String, Object>> results = db.queryMultiple(req2, currentTime);
        List<Pair<String, String>> data = new ArrayList<>();
        for(Map<String, Object> val : results){
            long date = (long) val.get("date");
            String titreSeance = DateUtils.formatJourMoisAnnee(date);
            //nombreDeReps, m.date, duration, tempsRepos
            int nombreDeReps = (int) val.get("nombreDeReps");
            int duration = (int) val.get("duration");
            int tempsRepos = (int) val.get("tempsRepos");

            String allData  = "Nombre de Reps: "+nombreDeReps+" | Durée "+DateUtils.formatHeureMinute(duration)+" | Temps Repos : "+DateUtils.formatMinuteSeconde(tempsRepos*1000);
            data.add(new Pair<>(titreSeance, allData));
        }


        this.liste.setAdapter(new AdapteurSeance(this, data));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_creationSeance:
                Intent createSeanceActivity = new Intent(StatisticActivity.this, CreateSeanceActivity.class);
                startActivity(createSeanceActivity);
                return true;
            case R.id.menu_statscourse:
                activityCourse();
                return true;
            case R.id.menu_statsmuscu:
                activityMuscu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
