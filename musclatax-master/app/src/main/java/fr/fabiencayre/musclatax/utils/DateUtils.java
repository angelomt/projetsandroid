package fr.fabiencayre.musclatax.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @author fabien
 */
public final class DateUtils {

    private static final DateFormat formatJMA = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
    private static final DateFormat formatHM = new SimpleDateFormat("HH 'h' mm 'm'", Locale.FRANCE);
    private static final DateFormat formatMS = new SimpleDateFormat("mm 'm' ss 's'", Locale.FRANCE);


    public static String formatJourMoisAnnee(long time){
        return formatJMA.format(time);
    }

    public static String formatMinuteSeconde(long time){return formatMS.format(time);}

    public static String formatHeureMinute(long time){
        return formatHM.format(time);
    }

}
