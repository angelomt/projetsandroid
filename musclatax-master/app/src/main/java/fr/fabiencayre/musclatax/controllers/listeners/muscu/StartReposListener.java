package fr.fabiencayre.musclatax.controllers.listeners.muscu;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

import fr.fabiencayre.musclatax.MuscuSeanceActivity;
import fr.fabiencayre.musclatax.R;
import fr.fabiencayre.musclatax.controllers.listeners.ActivityListener;
import fr.fabiencayre.musclatax.services.MuscuService;

public class StartReposListener extends ActivityListener implements View.OnClickListener {

    public StartReposListener(MuscuSeanceActivity activity) {
        super(activity);
    }

    @Override
    public void onClick(View v) {
        MuscuSeanceActivity activity = (MuscuSeanceActivity) super.activity;
        // On lance le service (create) et on le lie a l'activité (bind)
        System.out.println("A");
        if(activity.estEnRepos()){
            System.out.println("repos");
            return;
        }
        ((Button)v).setBackground(
                    activity.getResources().getDrawable(
                                R.drawable.background_round_red, activity.getTheme()));
        Intent intent = new Intent(activity, MuscuService.class);
        intent.putExtra("temps_repos", activity.getData().getTempsRepos());
        activity.startService(intent);
        activity.bindService(intent , activity.getServiceConnection(), Activity.BIND_AUTO_CREATE);
    }
}
