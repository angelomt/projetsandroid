package fr.fabiencayre.musclatax.controllers.listeners.createact;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import fr.fabiencayre.musclatax.CreateSeanceActivity;
import fr.fabiencayre.musclatax.controllers.listeners.ActivityListener;

public class LaunchSeanceListener extends ActivityListener implements View.OnClickListener {

    public LaunchSeanceListener(CreateSeanceActivity activity) {
        super(activity);
    }

    @Override
    public void onClick(View v) {
        CreateSeanceActivity activity = (CreateSeanceActivity) super.activity;
        if(activity.getOptions() == null)return;
        Intent laSeance = new Intent(activity, activity.getOptions().getActivity());
        laSeance.putExtra("options", activity.getOptions());
        activity.startActivityForResult(laSeance, 1);
    }
}
