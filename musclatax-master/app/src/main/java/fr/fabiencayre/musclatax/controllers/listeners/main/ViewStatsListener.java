package fr.fabiencayre.musclatax.controllers.listeners.main;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import fr.fabiencayre.musclatax.StatisticActivity;
import fr.fabiencayre.musclatax.controllers.listeners.ActivityListener;

public class ViewStatsListener extends ActivityListener implements View.OnClickListener {

    public ViewStatsListener(Activity activity) {
        super(activity);
    }

    @Override
    public void onClick(View v) {
        Intent stats = new Intent(this.activity, StatisticActivity.class);
        this.activity.startActivity(stats);
    }
}
