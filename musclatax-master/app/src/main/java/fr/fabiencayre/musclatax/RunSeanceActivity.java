package fr.fabiencayre.musclatax;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import fr.fabiencayre.musclatax.models.DownloadTask;
import fr.fabiencayre.musclatax.models.Seance;
import fr.fabiencayre.musclatax.services.CourseService;
import fr.fabiencayre.musclatax.utils.Pair;

public class RunSeanceActivity extends AppCompatActivity {

    GoogleMap googleMap;
    Service service;
    boolean isBound;
    List<Polyline> oldPolylines;

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CourseService.CourseBinder bridge = (CourseService.CourseBinder) service;
            RunSeanceActivity.this.service = bridge.getService();
            RunSeanceActivity.this.isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            RunSeanceActivity.this.isBound = false;
            RunSeanceActivity.this.service = null;
        }
    };

    BroadcastReceiver cheminCourse = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            List<List<Pair<Double, Double>>> pleinDePoints = (List<List<Pair<Double, Double>>>) intent.getSerializableExtra("allPoints");
            PolylineOptions lineOptions = new PolylineOptions();

            for(List<Pair<Double, Double>> lesLignes : pleinDePoints){
                List<LatLng> latitudes = new ArrayList<>();
                for(Pair<Double, Double> lesPoints : lesLignes){
                    latitudes.add(new LatLng(lesPoints.getV1(), lesPoints.getV2()));
                }
                lineOptions.addAll(latitudes);
                lineOptions.width(8);
                lineOptions.color(0xFF4a8cfc);
                lineOptions.geodesic(true);
            }
            /*
            if(!oldPolylines.isEmpty()){
                for(Polyline polyline : oldPolylines){
                    polyline.remove();
                }
            }
            */
            googleMap.addPolyline(lineOptions);

        }
    };

    BroadcastReceiver messageCourse = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            double[] latitudes = intent.getDoubleArrayExtra("latitudes");
            double[] longitudes = intent.getDoubleArrayExtra("longitudes");
            System.out.println("Accepting new positions");
            if(latitudes != null && longitudes != null){
                List<LatLng> positions = new ArrayList<>();
                for(int i = 0; i <  latitudes.length; i++){
                    positions.add(new LatLng(latitudes[i], longitudes[i]));
                }

                System.out.println("");
                // On doit le faire quand on reçoit les nouvelles lignes.
                if(!oldPolylines.isEmpty()){
                    for(Polyline polyline : oldPolylines){
                        polyline.remove();
                    }
                }
                if(positions.size() >= 2){
                    System.out.println("Requêtes");
                    List<String> urls = getDirectionsUrls(positions);
                    for(String url : urls){
                        DownloadTask downloadTask = new DownloadTask(RunSeanceActivity.this);
                        downloadTask.execute(url);
                    }
                }
                int maxSize = positions.size();
                LatLng lastPosition = positions.get(maxSize-1);
                System.out.println("Current camera positon : "+lastPosition);
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(lastPosition));
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_seance);

        this.oldPolylines = new ArrayList<>();

        ((SupportMapFragment) getSupportFragmentManager()
         .findFragmentById(R.id.map))
         .getMapAsync(googleMap -> {
             this.googleMap = googleMap;
             googleMap.setTrafficEnabled(false);
             System.out.println("La vue a été chargée : "+googleMap.getCameraPosition());
             googleMap.moveCamera(CameraUpdateFactory.zoomTo(17f));

        });

        askForPerms();



        Intent intent = new Intent(this, CourseService.class);
        this.startService(intent);
        this.bindService(intent , this.serviceConnection, Activity.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(messageCourse, new IntentFilter(CourseService.MESSAGE_COURSE));
        LocalBroadcastManager.getInstance(this).registerReceiver(cheminCourse, new IntentFilter(CourseService.MESSAGE_COURSE_CHEMIN));


        Seance seance = (Seance) getIntent().getSerializableExtra("Seance");


    }

    private void askForPerms(){
        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!permissionAccessCoarseLocationApproved) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }


        boolean permissionAccessFineLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!permissionAccessFineLocationApproved) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        // On re-écoute l'évènement
        LocalBroadcastManager.getInstance(this).registerReceiver(messageCourse, new IntentFilter(CourseService.MESSAGE_COURSE));
    }



    @Override
    protected void onPause() {
        // On ne lit plus l'évènement car l'activité est en arrière plan
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageCourse);
        super.onPause();
    }

    private List<String> getDirectionsUrls(List<LatLng> fromsTo){
        if(fromsTo.size() < 2){
            throw new IllegalArgumentException("Il n'y a pas assez de point.");
        }
        List<String> urls = new ArrayList<>();
        for(int i = 0; i < fromsTo.size() - 1; i++){
            LatLng from = fromsTo.get(i);
            LatLng to = fromsTo.get(i+1);
            String url = getDirectionsUrl(from, to);
            urls.add(url);
        }
        return urls;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=walking";
        // Building the parameters to the web service
        String key = "key="+getResources().getString(R.string.google_map_api);
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode+ "&"+key;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }
}
