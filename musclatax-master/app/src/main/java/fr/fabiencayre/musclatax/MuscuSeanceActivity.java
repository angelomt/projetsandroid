package fr.fabiencayre.musclatax;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import fr.fabiencayre.musclatax.controllers.listeners.createact.CourseData;
import fr.fabiencayre.musclatax.controllers.listeners.createact.MusculationData;
import fr.fabiencayre.musclatax.controllers.listeners.muscu.StartReposListener;
import fr.fabiencayre.musclatax.models.MuscuSeance;
import fr.fabiencayre.musclatax.models.Seance;
import fr.fabiencayre.musclatax.services.MuscuService;
import fr.fabiencayre.musclatax.utils.DateUtils;
import fr.fabiencayre.musclatax.utils.Utils;
import fr.fabiencayre.musclatax.utils.database.Database;

public class MuscuSeanceActivity extends AppCompatActivity {

    private MuscuService service;
    private boolean isBound;
    private MusculationData data;
    private int progression;

    ProgressBar repos;
    TextView reposValeur;
    TextView progressionValue;
    TextView progressionPerc;
    Button arretSeance;
    Button buttonMuscu;
    MuscuSeance seance;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MuscuService.MuscuBinder bridge = (MuscuService.MuscuBinder) service;
            MuscuSeanceActivity.this.service = bridge.getService();
            MuscuSeanceActivity.this.isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            MuscuSeanceActivity.this.isBound = false;
            MuscuSeanceActivity.this.service = null;
        }
    };

    private BroadcastReceiver messageRepos = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int repos = intent.getIntExtra("repos", -1);
            if(repos == -1)return;
            // On augmente le repos
            MuscuSeanceActivity zis = MuscuSeanceActivity.this;
            zis.repos.setProgress(repos);
            zis.reposValeur.setText(DateUtils.formatMinuteSeconde(repos*1000));
            // Si le repos est fini
            if(repos == MuscuSeanceActivity.this.data.getTempsRepos()){
                // Reset du repos + ajout d'une reps
                zis.progression++;
                zis.repos.setProgress(0);
                zis.progressionValue.setText(zis.progression+"/"+zis.data.getNombreDeReps());
                zis.reposValeur.setText("00:00");
                Toast.makeText(zis, R.string.repos_end, Toast.LENGTH_SHORT).show();


                String dispPerc = zis.getResources().getString(R.string.progression_seance);
                zis.progressionPerc.setText(dispPerc.replaceAll("%perc%", Utils.formatDouble(versPourcentage(progression))));

                zis.buttonMuscu
                        .setBackground(zis.getResources().getDrawable(R.drawable.background_round_green, zis.getTheme()));
                zis.unbindService(serviceConnection);
                zis.isBound = false;
                zis.service = null;
                zis.stopService(new Intent(zis, MuscuService.class));
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        // On re-écoute l'évènement
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(messageRepos, new IntentFilter(MuscuService.MESSAGE_REPOS));
    }



    @Override
    protected void onPause() {
        // On ne lit plus l'évènement car l'activité est en arrière plan
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(messageRepos);
        super.onPause();
    }

    public boolean estEnRepos(){
        if(this.isBound){
            return this.service.estEnRepos();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muscu_seance);
        Database database = new Database(this);

        this.seance = new MuscuSeance();
        seance.setDate(System.currentTimeMillis());
        seance.setDuration(0);

        this.data = (MusculationData) this.getIntent().getSerializableExtra("options");
        repos = (ProgressBar) findViewById(R.id.progressBarTime);
        repos.setMax(this.data.getTempsRepos());
        repos.setProgress(0);

        arretSeance = findViewById(R.id.stopSeance);

        progressionValue = findViewById(R.id.progressionValue);
        progressionPerc = findViewById(R.id.progressionPerc);
        reposValeur = findViewById(R.id.tempsRepos);


        buttonMuscu = findViewById(R.id.buttonMuscu);
        buttonMuscu.setOnClickListener(new StartReposListener(this));
        arretSeance.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.ask_stop_seance_muscu)
                    .setPositiveButton(R.string.yes, (dialog, which) -> {
                        // On renvoie le résultat à l'activité
                        // Main pour qu'elle enregistre dans la BDD
                        Intent resultatSeance = new Intent();
                        long start = seance.getDate();
                        long end = System.currentTimeMillis();
                        seance.setDuration(end-start);
                        seance.setNbReps(this.progression);
                        seance.setTempsRepos(this.data.getTempsRepos());
                        resultatSeance.putExtra("seance", seance);
                        this.setResult(Activity.RESULT_OK, resultatSeance);
                        //
                        if(this.isBound) {
                            this.unbindService(serviceConnection);
                            this.stopService(new Intent(this, MuscuService.class));
                        }
                        this.finish();

                    })
                    .setNegativeButton(R.string.no, (dialog, which) -> {});
            builder.create().show();
        });
        // Initialisation des affichages
        repos.setProgress(0);
        progressionValue.setText(progression+"/"+data.getNombreDeReps());
        reposValeur.setText("00:00");

        String dispPerc = getResources().getString(R.string.progression_seance);
        progressionPerc.setText(dispPerc.replaceAll("%perc%", String.valueOf(versPourcentage(progression))));

    }

    @Override
    // Visiblement, ça ne réapplique pas les modification
    // de background fait aux anciennes vues
    // Il faut donc le faire manuellement
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        System.out.println("CONFIG CHANGE T MORT");
        // L'orientation à changé
        if (estEnRepos()){
            this.buttonMuscu.setBackground(
                    this.getResources().getDrawable(
                            R.drawable.background_round_red, this.getTheme()));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(this.isBound) {
            this.unbindService(serviceConnection);
            this.stopService(new Intent(this, MuscuService.class));
        }
    }

    public double versPourcentage(int reps){
        double d = Double.valueOf(reps) / Double.valueOf(this.data.getNombreDeReps());
        return d*100;
    }

    public ServiceConnection getServiceConnection() {
        return serviceConnection;
    }

    public MusculationData getData() {
        return data;
    }
}
