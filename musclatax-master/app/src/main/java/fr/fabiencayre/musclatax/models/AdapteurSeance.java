package fr.fabiencayre.musclatax.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import fr.fabiencayre.musclatax.R;
import fr.fabiencayre.musclatax.utils.Pair;

public class AdapteurSeance extends BaseAdapter {

    private List<Pair<String, String>> datas;
    private LayoutInflater inflater;

    public AdapteurSeance(Context context, List<Pair<String, String>> datas){
        super();
        this.datas = datas;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.datas.size();
    }

    @Override
    public Pair<String, String> getItem(int position) {
        return this.datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View oldView = convertView;
        if(oldView == null){
            oldView = inflater.inflate(R.layout.row_data_seance, null);
        }
        TextView title = oldView.findViewById(R.id.titleSeance);
        TextView data = oldView.findViewById(R.id.dataSeance);

        Pair<String, String> dat = this.datas.get(position);
        title.setText(dat.getV1());
        data.setText(dat.getV2());

        return oldView;
    }
}
