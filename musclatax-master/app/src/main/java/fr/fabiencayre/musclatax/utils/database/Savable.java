package fr.fabiencayre.musclatax.utils.database;

import android.content.ContentValues;

/**
 * @author fabien
 *
 * La classe étendu doit avoir un constructeur comme ceci
 *
 * <pre>
 *     public Savable(Map<String, Object> map)
 * </pre>
 */
public interface Savable {

    void save(ContentValues values);

    int getID();

}
