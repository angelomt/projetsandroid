package fr.fabiencayre.musclatax.controllers.listeners.createact;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import fr.fabiencayre.musclatax.R;
import fr.fabiencayre.musclatax.RunSeanceActivity;

public class CourseData extends OptionData{

    private boolean notif;
    private int nbKm;

    public CourseData(String name) {
        super(name);
    }

    @Override
    public Class<? extends AppCompatActivity> getActivity() {
        return RunSeanceActivity.class;
    }

    @Override
    public void toggle(View layout) {
        EditText kmInput = layout.findViewById(R.id.nb_km);
        kmInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String chaine = s.toString();
                if(chaine.isEmpty()){
                    CourseData.this.nbKm = 0;
                }else{
                    CourseData.this.nbKm = Integer.parseInt(chaine);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        CheckBox notif = layout.findViewById(R.id.checkBox_distance);
        notif.setOnCheckedChangeListener((buttonView, isChecked) -> {
            CourseData.this.notif = isChecked;
        });

    }

    public boolean isNotif() {
        return notif;
    }

    public int getNbKm() {
        return nbKm;
    }
}
