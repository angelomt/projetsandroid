package fr.fabiencayre.musclatax.models;

import android.content.ContentValues;

import java.io.Serializable;
import java.util.Map;

import fr.fabiencayre.musclatax.utils.database.Database;
import fr.fabiencayre.musclatax.utils.database.Savable;

public abstract class Seance implements Savable, Serializable {

    private int id;
    private long date;
    private long duration;

    @SuppressWarnings("unused")
    // Seulement utilisé avec Reflection dans Database
    public Seance(Map<String, Object> map){
        this.id = (int) map.get("id");
        try{
            this.date = (long) map.get("date");
        }catch (Exception e){
            this.date = (int) map.get("date");
        }

        try{
            this.duration = (long) map.get("duration");
        }catch (Exception e){
            this.duration = (int) map.get("duration");
        }
        this.id = -1;
    }

    public Seance(){
        this.id = 0;
    }

    @Override
    public void save(ContentValues values) {
        values.put("date", this.date);
        values.put("duration", this.duration);
    }

    @Override
    public int getID() {
        return this.id;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getDate() {
        return date;
    }

    public long getDuration() {
        return duration;
    }


    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Seance{" +
                ", id=" + id +
                ", date=" + date +
                ", duration=" + duration +
                '}';
    }
}
