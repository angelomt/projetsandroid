package fr.fabiencayre.musclatax.models;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Looper;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.fabiencayre.musclatax.services.CourseService;
import fr.fabiencayre.musclatax.utils.DirectionsJSONParser;
import fr.fabiencayre.musclatax.utils.Pair;

public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

    private Context context;

    public ParserTask(Context context) {
        this.context = context;
    }

    // Parsing the data in non-ui thread
    @Override
    protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

        JSONObject jObject;
        List<List<HashMap<String, String>>> routes = null;

        try {
            jObject = new JSONObject(jsonData[0]);
            DirectionsJSONParser parser = new DirectionsJSONParser();

            routes = parser.parse(jObject);
            System.out.println("Le chemin:"+routes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return routes;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> result) {

        List<List<Pair<Double, Double>>> allPoints = new ArrayList<>();

        for(List<HashMap<String, String>> paths : result){
            List<Pair<Double, Double>>  subPath = new ArrayList<>();
            for(HashMap<String, String> values : paths){
                double latitude = Double.parseDouble(values.get("lat"));
                double longitude = Double.parseDouble(values.get("lng"));
                Pair<Double, Double> unPointSurLaMap = new Pair<>(latitude, longitude);
                subPath.add(unPointSurLaMap);
            }
            allPoints.add(subPath);
        }
        if(this.context != null){
            Intent intent = new Intent(CourseService.MESSAGE_COURSE_CHEMIN);
            intent.putExtra("allPoints", (Serializable) allPoints);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }

    }
}
