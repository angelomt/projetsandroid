package fr.fabiencayre.musclatax.controllers.listeners.createact;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.HashMap;
import java.util.Map;

import fr.fabiencayre.musclatax.CreateSeanceActivity;
import fr.fabiencayre.musclatax.R;
import fr.fabiencayre.musclatax.controllers.listeners.ActivityListener;

public class SelectTypeSeanceListener extends ActivityListener implements RadioGroup.OnCheckedChangeListener {

    private Map<String, Integer> layoutIdMap;
    private Map<String, Class<? extends OptionData>> optionMap;

    public SelectTypeSeanceListener(Activity activity) {
        super(activity);
        this.layoutIdMap = new HashMap<>();
        this.optionMap = new HashMap<>();
        String[] typeSeance = activity.getResources().getStringArray(R.array.type_seance);
        layoutIdMap.put(typeSeance[0], R.layout.muscu_option_layout);
        optionMap.put(typeSeance[0], MusculationData.class);

        layoutIdMap.put(typeSeance[1], R.layout.course_option_layout);
        optionMap.put(typeSeance[1], CourseData.class);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        CreateSeanceActivity activity = (CreateSeanceActivity) super.activity;

        RadioButton boutton = group.findViewById(checkedId);
        String name = boutton.getText().toString();

        int layoutId =  this.layoutIdMap.get(name);
        Class<? extends OptionData> clazzData = this.optionMap.get(name);
        try{
            OptionData data = clazzData.getConstructor(String.class).newInstance(name);

            activity.setOptions(data);
            View options = activity.getLayoutInflater().inflate(layoutId, (ViewGroup) boutton.getParent(), false);
            data.toggle(options);
            changeView(options);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void changeView(View newView){
        CreateSeanceActivity activity = (CreateSeanceActivity) super.activity;
        LinearLayout linearView = activity.findViewById(R.id.options_layout);
        if(linearView.getChildCount() != 0){
            linearView.removeAllViews();
        }
        linearView.addView(newView);

    }


}
