package fr.fabiencayre.musclatax.models;

import android.content.ContentValues;

import java.util.Map;

public class RunSeance extends Seance{

    private int nbKm;

    public RunSeance(Map<String, Object> map) {
        super(map);
        this.nbKm = (int) map.get("nbKm");
    }

    public RunSeance(){
        super();
    }

    public int getNbKm() {
        return nbKm;
    }

    public void setNbKm(int nbKm) {
        this.nbKm = nbKm;
    }

    @Override
    public void save(ContentValues values) {
        super.save(values);
        values.put("nbKm", this.nbKm);
    }

}
