package fr.fabiencayre.musclatax.services;

import android.app.IntentService;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Le service permettant de démarrer le temps de repos de la musculation
 * @author fabien
 */
public class MuscuService extends IntentService {

    public static final String MESSAGE_REPOS = "repos-actualiser";
    public static final String MESSAGE_FIN = "repos-fin";

    private final IBinder muscuBinder;

    public MuscuService() {
        super("MuscuService");
        this.muscuBinder = new MuscuBinder();
    }

    public class MuscuBinder extends Binder{

        public MuscuService getService(){
            return MuscuService.this;
        }

    }

    private class MuscuThread extends Thread {

        MuscuThread() {
            this.setDaemon(true);
        }

        @Override
        public void run() {
            MuscuService.this.estRepos = true;
            while(true){
                if(reposActuel < maxRepos){
                    reposActuel++;
                    try{
                        Thread.sleep(1000);
                    }catch (InterruptedException e){
                        break;
                    }
                    sendReposActualiser();
                }else{
                    break;
                }
            }
            MuscuService.this.estRepos = false;
        }
    }


    private boolean estRepos;
    private int maxRepos;
    private int reposActuel;
    private Thread processus;

    public void sendReposActualiser(){
        Intent intent = new Intent(MESSAGE_REPOS);
        intent.putExtra("repos", this.reposActuel);
        // Permet d'envoyer un message depuis un service en arrière plan
        // Vers une activité qui écoute l'intent "repos"
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public boolean estEnRepos(){
        return this.estRepos;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.processus.interrupt();
        this.processus = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        //Démarrage du Thread pour le repos
        this.maxRepos = intent.getIntExtra("temps_repos", 90);
        System.out.println("Le service à été bind (Max repos = "+maxRepos+")");
        this.processus = new MuscuThread();
        processus.start();
        return this.muscuBinder;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(intent != null){
            intent.getStringExtra("");
        }

    }
}
