package fr.fabiencayre.musclatax;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import fr.fabiencayre.musclatax.controllers.listeners.createact.LaunchSeanceListener;
import fr.fabiencayre.musclatax.controllers.listeners.createact.OptionData;
import fr.fabiencayre.musclatax.controllers.listeners.createact.SelectTypeSeanceListener;
import fr.fabiencayre.musclatax.models.MuscuSeance;
import fr.fabiencayre.musclatax.models.RunSeance;
import fr.fabiencayre.musclatax.models.Seance;
import fr.fabiencayre.musclatax.services.MuscuService;
import fr.fabiencayre.musclatax.utils.DateUtils;
import fr.fabiencayre.musclatax.utils.database.Database;

public class CreateSeanceActivity extends AppCompatActivity {

    /**
     * La liste des choix
     */
    RadioButton[] choix;
    TextView nomSeance;
    OptionData options;
    Button lancerSeance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // On lie la vue xml a l'activité
        setContentView(R.layout.activity_create_seance);

        // On cherche le groupe de radio button pour le remplir
        RadioGroup radioGroup = super.findViewById(R.id.select_type);
        this.nomSeance = super.findViewById(R.id.titreSeance);
        this.lancerSeance = super.findViewById(R.id.launchSeance);
        radioGroup.setOnCheckedChangeListener(new SelectTypeSeanceListener(this));
        // On récupère les différents type de séances
        String[] typeSeances = getResources().getStringArray(R.array.type_seance);
        this.choix = new RadioButton[typeSeances.length];
        int i = 0;

        for(String typeSeance : typeSeances){
            // On créer le radio
            RadioButton rButton = new RadioButton(this);
            rButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            rButton.setTextColor(getResources().getColor(R.color.black));
            // On le range dans notre   tableau de radio
            this.choix[i++] = rButton;
            rButton.setText(typeSeance);
            radioGroup.addView(rButton);
        }

        long tempActuel = System.currentTimeMillis();
        String nomSeanceFormat = getResources().getString(R.string.name_seance_format);
        String nomRemplace = nomSeanceFormat.replaceAll("%dateDMY%",
                DateUtils.formatJourMoisAnnee(tempActuel));
        this.nomSeance.setText(nomRemplace);

        this.lancerSeance.setOnClickListener(new LaunchSeanceListener(this));

    }

    public OptionData getOptions() {
        return options;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null){
            Seance seance = (Seance) data.getSerializableExtra("seance");
            if(seance instanceof MuscuSeance){
                new Database(this).save("Muscu", seance);
        }else if(seance instanceof RunSeance){
                new Database(this).save("Course", seance);
            }
            this.finish();
        }
    }

    public void setOptions(OptionData options) {
        this.options = options;
    }


}
