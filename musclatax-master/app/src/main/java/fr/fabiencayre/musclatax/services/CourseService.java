package fr.fabiencayre.musclatax.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import fr.fabiencayre.musclatax.utils.Pair;

public class CourseService extends IntentService implements LocationListener{

    public static final String MESSAGE_COURSE = "course";
    public static final String MESSAGE_COURSE_CHEMIN = "mapPath";

    private final IBinder muscuBinder;
    private LocationManager locationManager;

    private List<Pair<Double, Double>> locations;

    public CourseService() {
        super("CourseService");
        this.muscuBinder = new CourseService.CourseBinder();
        this.locations = new ArrayList<>();
    }

    public class CourseBinder extends Binder {

        public CourseService getService(){
            return CourseService.this;
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        this.locations.add(new Pair<>(location.getLatitude(), location.getLongitude()));
        updateLocations();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    public void updateLocations(){
        Intent intent = new Intent(MESSAGE_COURSE);
        double[] latitudes = new double[this.locations.size()];
        double[] longitudes = new double[this.locations.size()];
        for(int i = 0; i < this.locations.size(); i++){
            latitudes[i] = this.locations.get(i).getV1();
            longitudes[i] = this.locations.get(i).getV2();
        }
        intent.putExtra("latitudes", latitudes);
        intent.putExtra("longitudes", longitudes);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        //Démarrage du Thread pour le repos
        this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gpsEnabled = this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean networkEnabled = this.locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean passiveEnabled = this.locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

        try{
            if(passiveEnabled){
                this.locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 100, 5, this);
            }
            if(networkEnabled){
                this.locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 5, this);
            }
            if(gpsEnabled){
                this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 5, this);
            }
        }catch (SecurityException e){}
        return this.muscuBinder;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(intent != null){
            intent.getStringExtra("");
        }

    }
}
