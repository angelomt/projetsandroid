package fr.fabiencayre.musclatax.controllers.listeners;

import android.app.Activity;
import android.view.View;

public abstract class ActivityListener {

    protected Activity activity;

    public ActivityListener(Activity activity) {
        this.activity = activity;
    }
}
