package fr.fabiencayre.musclatax.controllers.listeners.main;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import fr.fabiencayre.musclatax.CreateSeanceActivity;
import fr.fabiencayre.musclatax.controllers.listeners.ActivityListener;

public class CreateSeanceListener extends ActivityListener implements View.OnClickListener {

    public CreateSeanceListener(Activity activity) {
        super(activity);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(super.activity, CreateSeanceActivity.class);
        super.activity.startActivity(intent);
    }
}
