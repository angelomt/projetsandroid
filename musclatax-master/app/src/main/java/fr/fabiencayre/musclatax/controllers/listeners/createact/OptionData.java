package fr.fabiencayre.musclatax.controllers.listeners.createact;

import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;

public abstract class OptionData implements Serializable {

    private String name;

    public OptionData(String name) {
        this.name = name;
    }

    public abstract Class<? extends AppCompatActivity> getActivity();

    public abstract void toggle(View layout);

    public String getName() {
        return name;
    }
}
