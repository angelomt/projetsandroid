package fr.fabiencayre.musclatax;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

import fr.fabiencayre.musclatax.controllers.listeners.main.CreateSeanceListener;
import fr.fabiencayre.musclatax.controllers.listeners.main.ViewStatsListener;

public class MainActivity extends AppCompatActivity {

    Button createSeance;
    Button statsSeance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.createSeance = findViewById(R.id.createSeance);
        this.statsSeance = findViewById(R.id.viewStats);

        this.createSeance.setOnClickListener(new CreateSeanceListener(this));
        this.statsSeance.setOnClickListener(new ViewStatsListener(this));
    }


}
