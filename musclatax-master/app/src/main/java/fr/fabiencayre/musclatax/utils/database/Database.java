package fr.fabiencayre.musclatax.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.fabiencayre.musclatax.R;

/**
 * @author fabien
 */
public class Database extends SQLiteOpenHelper {

    private static final int CURRENT_VERSION = 2;
    public static final String DATABASE_NAME = "musclatax.db";

    private List<String> createSeanceQuery;

    public Database(@Nullable Context context){
        super(context, DATABASE_NAME, null, CURRENT_VERSION);
        this.createSeanceQuery = new ArrayList<>();
        this.createSeanceQuery.add(context.getResources().getString(R.string.SQL_CREATE_TABLE_MUSCU));
        this.createSeanceQuery.add(context.getResources().getString(R.string.SQL_CREATE_TABLE_COURSE));
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for(String query : this.createSeanceQuery){
            db.execSQL(query);
        }
    }

    protected SQLiteDatabase getDatabase(){
        return super.getWritableDatabase();
    }

    public boolean existDatabase(String name){
        SQLiteDatabase db = getDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM sqlite_master WHERE type='table';", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String tableName = cursor.getString(1);
            if (!tableName.equals("android_metadata") &&
                    !tableName.equals("sqlite_sequence"))
                if(tableName.equals(name)){
                    cursor.close();
                    db.close();
                    return true;
                }
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return false;
    }

    public void dropTable(final String tableName){
        doActionOnDb(db ->  {
            db.execSQL("DROP TABLE IF EXISTS "+tableName);
        });
    }

    public void dropTables() {

        doActionOnDb(db -> {
            List<String> tables = new ArrayList<String>();
            Cursor cursor = db.rawQuery("SELECT * FROM sqlite_master WHERE type='table';", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                String tableName = cursor.getString(1);
                if (!tableName.equals("android_metadata") &&
                        !tableName.equals("sqlite_sequence"))
                    tables.add(tableName);
                cursor.moveToNext();
            }
            cursor.close();

            for (String table : tables) db.execSQL("DROP TABLE IF EXISTS "+table);
        });
    }

    public <T extends Savable> T loadSingle(Class<T> savable, String query, Object... params)
            throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Map<String, Object> map = querySingle(query, params);
        return savable
                .getConstructor(Map.class)
                .newInstance(map);
    }

    public <T extends Savable> List<T> loadMultiple(Class<T> savable, String query, Object... params){
        List<Map<String, Object>> list = queryMultiple(query, params);
        List<T> dataList = new ArrayList<>();
        try{
            for(Map<String, Object> map : list){
                Constructor<T> constructor = savable.getConstructor(Map.class);
                constructor.setAccessible(true);
                T instance = constructor.newInstance(map);
                dataList.add(instance);
            }
            return dataList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return dataList;
    }

    public int lastId(String tableName){
        SQLiteDatabase dataBase = getDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT id FROM "+tableName+";", new String[]{});
        if(cursor.getPosition() == -1){
            return 0;
        }
        cursor.moveToLast();
        Map<String, Object> map = new HashMap<>();
        for(String colName : cursor.getColumnNames()){
            int index = cursor.getColumnIndex(colName);
            map.put(colName, typeToObj(cursor, index));
        }
        cursor.close();
        dataBase.close();
        return (int) map.get("id");
    }

    public void erase(String tableName, Savable savable){
        this.rawQuery("DELETE FROM "+tableName+" WHERE id = ?", savable.getID());
    }

    public void save(Savable savable){
        String className = savable.getClass().getSimpleName();
        String clearName = className.substring(0)+className.substring(0, className.length()).toLowerCase();
        save(clearName, savable);
    }

    /**
     * Ouvre une connexion à la base de donnée, effectue l'action puis ferme la connexion
     * @param consumer L'action à effectué
     */
    public void doActionOnDb(Consumer<SQLiteDatabase> consumer){
        SQLiteDatabase dataBase = getDatabase();
        dataBase.beginTransaction();
        consumer.accept(dataBase);
        dataBase.setTransactionSuccessful();
        dataBase.endTransaction();
        dataBase.close();
    }

    public void save(final String name, Savable savable){
        final ContentValues value = new ContentValues();
        savable.save(value);
        doActionOnDb(db -> {
            db.insertOrThrow(name, null, value);
        });
    }



    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables();
        onCreate(db);
    }

    public void rawQuery(String sqlQuery, Object... params){
        doActionOnDb(db -> {
            db.execSQL(sqlQuery+";", params);
        });

    }

    private String[] objToStr(Object... params){
        String[] args = null;
        if(params != null && params.length > 0){
            args = new String[params.length];
            for(int i = 0 ; i < params.length; i++){
                args[i] = params[i].toString();
            }
        }
        return args;
    }

    public List<Map<String, Object>> queryMultiple(String sqlQuery, Object... params){
        SQLiteDatabase dataBase = getDatabase();
        Cursor cursor = dataBase.rawQuery(sqlQuery+";", objToStr(params));
        cursor.moveToFirst();
        List<Map<String, Object>> list = new ArrayList<>();
        while(!cursor.isAfterLast()){
            Map<String, Object> map = new HashMap<>();
            for(String colName : cursor.getColumnNames()){
                int index = cursor.getColumnIndex(colName);
                map.put(colName, typeToObj(cursor, index));
            }
            list.add(map);
            cursor.moveToNext();
        }
        cursor.close();
        dataBase.close();
        return list;
    }

    private Object typeToObj(Cursor cursor, int tableIndex){
        int type = cursor.getType(tableIndex);
        switch(type){
            case Cursor.FIELD_TYPE_NULL:
                return null;
            case Cursor.FIELD_TYPE_INTEGER :
                // C'est peut être un long !!!
                long l = cursor.getLong(tableIndex);
                try{
                    return Integer.parseInt(String.valueOf(l));
                }catch (Exception e){
                    return l;
                }
            case Cursor.FIELD_TYPE_FLOAT :
                return cursor.getDouble(tableIndex);
            case Cursor.FIELD_TYPE_STRING:
                return cursor.getString(tableIndex);
            case Cursor.FIELD_TYPE_BLOB:
                return cursor.getBlob(tableIndex);
        }
        return null;
    }

    public Map<String, Object> querySingle(String sqlQuery, Object... params){
        SQLiteDatabase dataBase = getDatabase();
        Cursor cursor = dataBase.rawQuery(sqlQuery+";", objToStr(params));
        cursor.moveToFirst();
        Map<String, Object> map = new HashMap<>();
        for(String colName : cursor.getColumnNames()){
            int index = cursor.getColumnIndex(colName);
            map.put(colName, typeToObj(cursor, index));
        }
        cursor.close();
        dataBase.close();
        return map;
    }

}
