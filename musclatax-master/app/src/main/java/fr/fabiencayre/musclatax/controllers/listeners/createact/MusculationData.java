package fr.fabiencayre.musclatax.controllers.listeners.createact;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import fr.fabiencayre.musclatax.MuscuSeanceActivity;
import fr.fabiencayre.musclatax.R;
import fr.fabiencayre.musclatax.utils.DateUtils;

/**
 *
 * Classe représentant les options de la séance de muscu
 * (Temps de repos entre chaque série)
 * (Nombre de répétition de la séance)
 * @author Fabien
 */
public class MusculationData extends OptionData {

    private static final int MIN_REPS = 10;
    private static final int STEP_REPS = 5;

    private static final int MIN_REPOS = 60;
    private static final int STEP_REPOS = 30;

    private int nombreDeReps;
    private int tempsRepos;

    public MusculationData(String name) {
        super(name);
    }

    @Override
    public Class<? extends AppCompatActivity> getActivity() {
        return MuscuSeanceActivity.class;
    }

    @Override
    public void toggle(View layout) {
        SeekBar nbRepsBar = layout.findViewById(R.id.nbRepsBar);
        TextView valueReps = layout.findViewById(R.id.displayNbReps);

        MusculationData.this.nombreDeReps = progressVersNbReps(nbRepsBar.getProgress());
        valueReps.setText(String.valueOf(MusculationData.this.nombreDeReps)+" reps");

        nbRepsBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                MusculationData.this.nombreDeReps = progressVersNbReps(progress);
                valueReps.setText(String.valueOf(MusculationData.this.nombreDeReps)+" reps");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        SeekBar tpsRepos = layout.findViewById(R.id.tpsReposBar);
        TextView valueRepos = layout.findViewById(R.id.displayTpsRepos);

        MusculationData.this.tempsRepos = progressVersTpsRepos(tpsRepos.getProgress());
        valueRepos.setText(DateUtils.formatMinuteSeconde(MusculationData.this.tempsRepos*1000L));

        tpsRepos.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                MusculationData.this.tempsRepos = progressVersTpsRepos(progress);
                valueRepos.setText(DateUtils.formatMinuteSeconde(MusculationData.this.tempsRepos*1000L));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    public int getNombreDeReps() {
        return nombreDeReps;
    }

    public int getTempsRepos() {
        return tempsRepos;
    }

    public int progressVersTpsRepos(int progress){
        return STEP_REPOS * progress + MIN_REPOS;
    }

    public int progressVersNbReps(int progress){
        return STEP_REPS * progress + MIN_REPS;
    }

}
