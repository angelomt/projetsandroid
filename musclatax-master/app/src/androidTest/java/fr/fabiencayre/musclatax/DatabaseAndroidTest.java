package fr.fabiencayre.musclatax;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

import static org.junit.Assert.*;

import fr.fabiencayre.musclatax.utils.database.Database;

@RunWith(AndroidJUnit4.class)
public class DatabaseAndroidTest {

    Database dataBase;

    public DatabaseAndroidTest(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        dataBase = new Database(appContext);
    }

    @Test
    public void testCreateDB(){

        dataBase.dropTables();
        String createSql = "CREATE TABLE Test (id INTEGER PRIMARY KEY, name VARCHAR(50))";

        dataBase.rawQuery(createSql);

        assertTrue(dataBase.existDatabase("Test"));
    }

    @Test
    public void testInsertData(){


        dataBase.save("Test", new TestSavable(1, "Jean-Michel"));

        Map<String, Object> map = dataBase.querySingle("SELECT id,name FROM Test WHERE id = ?", 1);
        int id = (int) map.get("id");
        String name = (String) map.get("name");

        assertEquals(id, 1);
        assertEquals(name, "Jean-Michel");
    }

    @Test
    public void testSavableClass(){
        TestSavable test = new TestSavable(2, "Robert");
        dataBase.save("Test", test);

        Map<String, Object> map = dataBase.querySingle("SELECT id,name FROM Test WHERE id = ?", 2);
        int id = (int) map.get("id");
        String name = (String) map.get("name");

        assertEquals(id, 2);
        assertEquals(name, "Robert");
    }

    @Test
    public void testLoadSavableClass() throws Exception{

        TestSavable test = new TestSavable(2, "Robert");
        dataBase.erase("Test", test);
        dataBase.save("Test", test);

        TestSavable loadTest = dataBase.loadSingle(TestSavable.class,
                                         "SELECT id,name FROM Test WHERE id = ?",
                                       2);
        assertNotNull(loadTest);
        assertEquals(2, loadTest.id);
        assertEquals("Robert", loadTest.nom);

        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        appContext.deleteDatabase(Database.DATABASE_NAME);
    }





}
