package fr.fabiencayre.musclatax;

import android.content.ContentValues;
import android.util.Log;

import java.util.Map;

import fr.fabiencayre.musclatax.utils.database.Savable;

public class TestSavable implements Savable {

    int id;
    String nom;

    public TestSavable(Map<String, Object> map){
        if(map.isEmpty()){
            Log.d("", "La map est vide");
        }
        this.id = (int) map.get("id");
        this.nom = (String) map.get("name");
    }

    public TestSavable(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    @Override
    public int getID() {
        return this.id;
    }

    @Override
    public void save(ContentValues values) {
        values.put("id", this.id);
        values.put("name", this.nom);
    }
}
