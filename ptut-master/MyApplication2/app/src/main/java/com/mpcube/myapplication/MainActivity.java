package com.mpcube.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.view.MenuItemCompat;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentProviderClient;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

import static com.mpcube.myapplication.BluetoothThread.*;

public class MainActivity extends AppCompatActivity {
    private SeekBar seekVert, seekRouge;
    private TextView textVert, textRouge, textEtat;
    private Switch link;
    private EditText setterRouge, setterVert;
    private RadioButton btnVert, btnChantier, btnRouge;
    private RadioGroup etatFeu;
    private Switch darkm;
    private MenuItem menu;

    private BluetoothAdapter bluetoothAdapter;
    private States messageAEnvoyer;
    private String mac;
    private BluetoothDevice device;
    private BluetoothSocket commSocket;

    private static final String PREF_NAME = "prefs";
    private static final String PREF_DARK_THEME = "theme";

    //méthodes permettant la gestion de la connexion bluetooth
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == RESULT_CANCELED) {
                runOnUiThread(()-> Toast.makeText(this, "vous avez refusé l'activation du bluetooth.", Toast.LENGTH_SHORT).show());
            }
        } catch (Exception ex) {
            runOnUiThread(()-> Toast.makeText(this, ex.toString(),
                    Toast.LENGTH_SHORT).show());
        }
        if(requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                this.mac = data.getStringExtra("device");

                BluetoothThread.getInstance().ajouterTache(this::connexion);
            }
        }
    }

    public void connectDevice(){
        Intent list = new Intent(this, BluetoothList.class);
        startActivityForResult(list, 1);
    }

    public void sendMsg(){
        try {
            DataOutputStream out = new DataOutputStream(commSocket.getOutputStream());
            out.writeBytes("$"+messageAEnvoyer.getEtat()+messageAEnvoyer.getTimerRouge()+"/"+messageAEnvoyer.getTimerVert()+":");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void connexion(){
        runOnUiThread(()-> Toast.makeText(this, "Connexion en cours...", Toast.LENGTH_SHORT).show());
        try{
            this.device = this.bluetoothAdapter.getRemoteDevice("00:14:03:05:58:FC");
            UUID CONNUUID = this.device.getUuids()[0].getUuid();
            BluetoothSocket bs = this.device.createInsecureRfcommSocketToServiceRecord(CONNUUID);
            bs.connect();
            if(!bs.isConnected()){
                runOnUiThread(()-> Toast.makeText(this, "l'appareil n'a pa pu se connecter au feu tricolore", Toast.LENGTH_SHORT).show());
                this.finish();
            }
            runOnUiThread(()-> Toast.makeText(this, "Feu tricolore connecté.", Toast.LENGTH_SHORT).show());
            this.commSocket = bs;
        }catch (Exception e){
            e.printStackTrace();
            this.finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity, menu);
        this.menu = menu.findItem(R.id.app_bar_switchDarkMode);
        this.menu.setActionView(R.layout.switch_item);
        this.darkm = this.menu.getActionView().findViewById(R.id.switch_id);
        if(getSharedPreferences(PREF_NAME, MODE_PRIVATE).getInt(PREF_DARK_THEME, R.style.AppTheme_Light) != R.style.AppTheme_Light) {
            this.darkm.setChecked(true);
        }
        this.darkm.setOnCheckedChangeListener((buttonView, isChecked) -> {
            int id;
            SharedPreferences.Editor editor = getSharedPreferences(PREF_NAME, MODE_PRIVATE).edit();
            if (isChecked) {
                id = R.style.AppTheme_Dark;
            } else {
                id = R.style.AppTheme_Light;
            }
            editor.putInt(PREF_DARK_THEME, id);
            editor.apply();

            finish();
            overridePendingTransition(0, 0);
            startActivity(getIntent());
            overridePendingTransition(0,0);
        });
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //dark theme
        SharedPreferences preferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        int themeUsed = preferences.getInt(PREF_DARK_THEME, R.style.AppTheme_Light);
        this.setTheme(themeUsed);
        setContentView(R.layout.activity_main);

        messageAEnvoyer = new States(60,60,'V');
        textVert = findViewById(R.id.textVert);
        textRouge = findViewById(R.id.textRouge);
        textEtat = findViewById(R.id.textEtat);
        connectDevice();
        BluetoothThread.getInstance().ajouterTache(this::initBluetooth);

        link = findViewById(R.id.link);

        seekVert = findViewById(R.id.seekvert);
        seekVert.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String print = String.valueOf(seekVert.getProgress());
                textVert.setText("Temps vert : "+print+" s");
                if (link.isChecked()){
                    seekRouge.setProgress(seekVert.getProgress());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        seekRouge = findViewById(R.id.seekrouge);
        seekRouge.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String print = String.valueOf(seekRouge.getProgress());
                textRouge.setText("Temps rouge : "+print+" s");
                if (link.isChecked()){
                    seekVert.setProgress(seekRouge.getProgress());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        setterVert = findViewById(R.id.setterVert);
        setterVert.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    int tps = Integer.parseInt(setterVert.getText().toString());
                    if (tps < 5){
                        tps = 5;
                        tps = Integer.parseInt(setterVert.getText().toString());
                    }
                    if (tps > 60){
                        tps = 60;
                        tps = Integer.parseInt(setterVert.getText().toString());
                    }
                    seekVert.setProgress(tps);
                }

                catch (NumberFormatException e){
                    e.printStackTrace();
                    seekVert.setProgress(5);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    int tps = Integer.parseInt(setterVert.getText().toString());
                    if (tps < 5){
                        setterVert.setText("5");
                        tps = Integer.parseInt(setterVert.getText().toString());
                    }
                    seekVert.setProgress(tps);
                }catch(NumberFormatException e){
                    e.printStackTrace();
                    seekVert.setProgress(5);
                }

            }
        });

        setterRouge = findViewById(R.id.setterRouge);
        setterRouge.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try{
                    int tps = Integer.parseInt(setterRouge.getText().toString());
                    if (tps > 60){
                        tps = 60;
                    }
                    seekRouge.setProgress(tps);
                }
                catch (NumberFormatException e){
                    e.printStackTrace();
                    seekRouge.setProgress(5);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    int tps = Integer.parseInt(setterRouge.getText().toString());
                    if (tps < 5){
                        tps = 5;
                    }
                    seekRouge.setProgress(tps);
                }catch(NumberFormatException e){
                    e.printStackTrace();
                    seekRouge.setProgress(5);
                }

            }
        });

        btnVert = findViewById(R.id.btnVert);
        btnChantier = findViewById(R.id.btnChantier);
        btnRouge = findViewById(R.id.btnRouge);
        etatFeu = findViewById(R.id.etatFeu);

        //changement d'état au check du radio button
        etatFeu.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.btnVert) {
                textEtat.setText("Etat du feu : Vert");
                messageAEnvoyer.setEtat('V');
                messageAEnvoyer.setTimerRouge(seekRouge.getProgress());
                messageAEnvoyer.setTimerVert(seekVert.getProgress());
                BluetoothThread.getInstance().ajouterTache(this::sendMsg);

            } else if(checkedId == R.id.btnChantier){
                textEtat.setText("Etat du feu : Chantier");
                messageAEnvoyer.setEtat('O');
                messageAEnvoyer.setTimerRouge(seekRouge.getProgress());
                messageAEnvoyer.setTimerVert(seekVert.getProgress());
                BluetoothThread.getInstance().ajouterTache(this::sendMsg);

            } else {
                textEtat.setText("Etat du feu : Rouge");
                messageAEnvoyer.setEtat('R');
                messageAEnvoyer.setTimerRouge(seekRouge.getProgress());
                messageAEnvoyer.setTimerVert(seekVert.getProgress());
                BluetoothThread.getInstance().ajouterTache(this::sendMsg);
            }
        });
    }

    public void initBluetooth(){
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        final int REQUEST_ENABLE_BT = 1;

        if (this.bluetoothAdapter == null) {
            runOnUiThread(()-> Toast.makeText(this, "Votre appareil ne dispose pas de bluetooth.", Toast.LENGTH_SHORT).show());
            finish();
        }
        this.bluetoothAdapter.cancelDiscovery();
        if(!this.bluetoothAdapter.isEnabled()){
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            onActivityResult(REQUEST_ENABLE_BT, RESULT_OK, null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            this.commSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
