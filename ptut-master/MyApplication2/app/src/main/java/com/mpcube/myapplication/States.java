package com.mpcube.myapplication;

public class States {
    private int timerRouge;
    private int timerVert;
    private char etat;

    public void setTimerRouge(int timerRouge) {
        this.timerRouge = timerRouge;
    }

    public void setTimerVert(int timerVert) {
        this.timerVert = timerVert;
    }

    public char getEtat() {
        return etat;
    }

    public void setEtat(char etat) {
        this.etat = etat;
    }

    public States(int timerRouge, int timerVert, char etat) {
        this.timerRouge = timerRouge;
        this.timerVert = timerVert;
        this.etat = etat;
    }

    public int getTimerRouge(){
        return timerRouge;
    }

    public int getTimerVert(){
        return timerVert;
    }

    public void nextState(){
        if(etat == 'R'){
            setEtat('V');
        }else if(etat == 'V'){
            setEtat('R');
        }
    }
}
