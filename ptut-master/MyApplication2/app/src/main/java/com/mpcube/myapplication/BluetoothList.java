package com.mpcube.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.UUID;

public class BluetoothList extends AppCompatActivity {

    private BluetoothAdapter bluetoothAdapter;
    private Set<BluetoothDevice> pairedDevices;
    private LinearLayout lay;
    private String mac;
    private static final String PREF_NAME = "prefs";
    private static final String PREF_DARK_THEME = "theme";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //dark theme
        SharedPreferences preferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        int themeUsed = preferences.getInt(PREF_DARK_THEME, R.style.AppTheme_Light);
        this.setTheme(themeUsed);
        setContentView(R.layout.activity_bluetooth_list);
        lay = findViewById(R.id.lay);

        BluetoothThread.getInstance().ajouterTache(this::initBluetooth);


    }

    public void initBluetooth(){
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        System.out.println("act bt");
        final int REQUEST_ENABLE_BT = 1;

        if (this.bluetoothAdapter == null) {
            runOnUiThread(()-> Toast.makeText(this, "Votre appareil ne dispose pas de bluetooth.", Toast.LENGTH_SHORT).show());
            finish();
        }
        this.bluetoothAdapter.cancelDiscovery();
        if(!this.bluetoothAdapter.isEnabled()){
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            onActivityResult(REQUEST_ENABLE_BT, RESULT_OK, null);
        }
        this.pairedDevices = this.bluetoothAdapter.getBondedDevices();
        System.out.println("fini init");

        System.out.println(pairedDevices);
        for(BluetoothDevice str : pairedDevices){
            Button text = new Button(this);
            text.setTextSize(20f);
            text.setText(str.getName());
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mac = str.getAddress();
                    System.out.println("mac : "+mac);
                    Intent returnIntent = getIntent();
                    returnIntent.putExtra("device", mac);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            });
            runOnUiThread(()->BluetoothList.this.lay.addView(text));
        }
    }
}
