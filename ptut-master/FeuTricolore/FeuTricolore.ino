#include<SoftwareSerial.h>

SoftwareSerial hc06(0, 1);

#define ROUGE 10
#define ORANGE 11
#define VERT 12
#define P_ROUGE 8
#define P_VERT 9
#define MAX_MESSAGE_SIZE 10
#define MIN_MESSAGE_SIZE 6

enum etat{
  travaux, vert, rouge
};

char command[MAX_MESSAGE_SIZE];
int commandSize;

// ETAT FEU TRICOLORE
int timerRouge;
int timerVert;
etat etatFeu;
unsigned long timeDebutAnimation;

void setup() {
  // initialise le module Bluetooth, et définit les branchements de leds comme sorties
  hc06.begin(9600);
  hc06.setTimeout(500); // pour éviter de rester bloqué plus de 500ms par une fonction de Serial

  pinMode (ROUGE, OUTPUT);
  pinMode (ORANGE, OUTPUT);
  pinMode (VERT, OUTPUT);
  pinMode (P_ROUGE, OUTPUT);
  pinMode (P_VERT, OUTPUT);
  digitalWrite (ROUGE, LOW);
  digitalWrite (ORANGE, LOW);
  digitalWrite (VERT, LOW);
  digitalWrite (P_ROUGE, LOW);
  digitalWrite (P_VERT, LOW);

  timerVert = 10000; // 10s par défaut
  timerRouge = 10000;
  etatFeu = travaux;
  commandSize = 0;
  timeDebutAnimation = millis();
}

void loop() {

  lireBluetooth();

    switch(etatFeu){
      case rouge :
        if (millis() - timeDebutAnimation <= 2000) {

            digitalWrite (VERT, LOW);
            digitalWrite(ORANGE, HIGH);
        } else if (millis() - timeDebutAnimation <= 4000) {

            digitalWrite(ORANGE, LOW);
            digitalWrite(ROUGE, HIGH);
        } else {

            digitalWrite (P_ROUGE, LOW);
            digitalWrite (P_VERT, HIGH);
        }
        if (millis() - timeDebutAnimation >= timerRouge +2000) {

            etatFeu = vert;
            timeDebutAnimation = millis(); // reset de l'animation
        }
        break;

      case vert :
          digitalWrite (ORANGE, LOW);
      if (millis() - timeDebutAnimation <= 3000) {

          digitalWrite (P_ROUGE, HIGH);
          digitalWrite (P_VERT, LOW);
      } else {

          digitalWrite (ROUGE, LOW);
          digitalWrite (VERT, HIGH);
      }
      if (millis() - timeDebutAnimation >= timerVert +3000) {

          etatFeu = rouge;
          timeDebutAnimation = millis(); // reset de l'animation
      }
      break;

      case travaux :

      if (millis() - timeDebutAnimation <= 500) {

          digitalWrite (ROUGE, LOW);
          digitalWrite (ORANGE, HIGH);
          digitalWrite (VERT, LOW);
          digitalWrite (P_ROUGE, LOW);
          digitalWrite (P_VERT, LOW);
      } else if (millis() - timeDebutAnimation <= 1000) {

          digitalWrite(ORANGE, LOW);
      } else {
          timeDebutAnimation = millis(); // reset de l'animation
      }
      break;
    }
}

void lireBluetooth() {

    //$X000/111: => format de la commande
    if (hc06.available() >= MIN_MESSAGE_SIZE) {

        // for Reference : https://www.arduino.cc/reference/en/language/functions/communication/serial/readbytesuntil/
        commandSize = hc06.readBytesUntil(':', command, MAX_MESSAGE_SIZE); // lis Serial dans command

        //debug
        hc06.println(command);
        
        if (commandSize >= MIN_MESSAGE_SIZE-1 && command[0] == '$') { // message semble valide

                int compteur = commandSize-1; // on commence par la fin pour savoir où on en est dans les unitées/dizaines/centaines
                timerRouge = 0;
                timerVert = 0;
                while (command[compteur] != '/' && compteur > 1) {
                    timerVert += (command[compteur] -48) * int(pow(10, commandSize-1 - compteur));
                    compteur --;
                }

                int positionSeparateur = compteur; // position du '/'
                compteur --; // on se positionne juste après le '/'
                while (compteur > 1) {
                    timerRouge += (command[compteur] -48) * int(pow(10, positionSeparateur-1 - compteur));
                    compteur --;
                }

                switch (command[1]) {
                    case 'R':
                        etatFeu = rouge;
                        break;
                    case 'V':
                        etatFeu = vert;
                        break;
                    case 'O':
                        etatFeu = travaux;
                        break;
                }

                /*Serial.print("Commande reçue: ");
                Serial.print(command[1]);
                Serial.print(" timerVert = ");
                Serial.print(timerVert);
                Serial.print(" timerRouge = ");
                Serial.println(timerRouge);*/

                timerVert *= 1000;
                timerRouge *= 1000;
                timeDebutAnimation = millis(); // on restart les animations

        }
    }
}
